import json


def save_json(json_file_path, json_array = []):
    #convert python json_array to JSON String and write to file
    with open(json_file_path, 'w', encoding='utf-8') as jsonf: 
        jsonString = json.dumps(json_array, indent=4)
        jsonf.write(jsonString)