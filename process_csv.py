import csv
from typing import List


def read_csv(csv_file_path) -> List[dict]:
    jsonArray = []
    #read csv file
    with open(csv_file_path, encoding='utf-8') as csvf: 
        #load csv file data using csv library's dictionary reader
        csvReader = csv.DictReader(csvf) 

        #convert each csv row into python dict
        for row in csvReader: 
            jsonArray.append(row)
    
    return jsonArray


