from datetime import datetime, timedelta
from typing import List
from slugify import slugify
import bcrypt

from process_mongo import get_database
from process_csv import read_csv
from process_json import save_json
from options import (
    features, eventTypes, orderingMethods, 
    onlineBasedMerchantTypes, onlineBasedServiceTypes, 
    pointOfSale, catererServiceTypes, catererType, 
    productCategories, suppliesServiceTypes,
    placeTypes, diningTypes,
    deliveryLocations, bakeType
)


def process_merchant_listings(db, input_data_arr) -> List[dict]:
    brands_col = db['brands']
    users_col = db['users']
    malls_col = db['malls']
    tags_col = db['tags']
    awards_col = db['awards']
    output_data_arr = []
    for row in input_data_arr:
        obj = {}
        social = {}
        links = {}
        verification = {}
        reward = {}
        tags = {}
        options = {}
        for key in row.keys():
            if key == "Status":
                obj.update({"status": row[key]})
            if key == "Listing Title":
                obj.update({
                    "title": row[key],
                    "slug": slugify(row[key], separator="_")
                })
            if key == "Brand":
                doc = brands_col.find_one({"title": row[key]})
                obj.update({'brand': None})
                if doc is not None:
                    obj.update({'brand': doc['_id']})
            if key == "Category":
                obj.update({"category": row[key]})
            if key == "Merchant":
                merchant_profile = str(row[key]).split(" - ")
                business_name = merchant_profile[0]
                contact_person = ''
                if len(merchant_profile) > 1:
                    contact_person = merchant_profile[1]
                doc = users_col.find_one({
                    'merchantProfile.businessName': business_name, 
                    'merchantProfile.contactPerson': contact_person
                })
                if doc is not None:
                    obj.update({'owner': doc["_id"]})
            if key == "Expiry Date":
                split_date = str(row[key]).split(" ")
                day = split_date[0].replace("st", "").replace("nd", "").replace("rd", "").replace("th", "")
                reformat_date = f"{day} {split_date[1]} {split_date[2]}"
                try:
                    date = datetime.strptime(reformat_date, '%d %B %Y')
                    obj.update({"expiryDate": date.isoformat()})
                except ValueError:
                    pass
            if key == "Mall":
                doc = malls_col.find_one({"title": row[key]})
                if doc is not None:
                    obj.update({'mallId': doc["_id"]})
            if key == "Street":
                if row[key] != "":
                    obj.update({'street': row[key]})
            if key == "Country":
                if row[key] != "":
                    obj.update({'country': row[key]})
            if key == "State":
                if row[key] != "":
                    obj.update({'state': row[key]})
            if key == "City":
                if row[key] != "":
                    obj.update({'city': row[key]})
            if key == "Coordinates":
                if row[key] != "":
                    obj.update({
                        'address': {
                            'coordinates': [
                                float(str(row[key]).split(", ")[0]),
                                float(str(row[key]).split(", ")[1]),
                            ],
                            'type': 'Point'
                        }
                    })
            if key == "General Email":
                if row[key] != "":
                    social.update({'email': row[key]})
                    # social['email'] = row[key]
            if key == "General Contact Number":
                if row[key] != "":
                    social.update({'contact': row[key]})
                    # social['contact'] = row[key]
            if key == "Website":
                if row[key] != "":
                    social.update({'website': row[key]})
                    # social['website'] = row[key]
            if key == "Facebook Page":
                if row[key] != "":
                    social.update({'facebook': row[key]})
                    # social['facebook'] = row[key]
            if key == "Instagram Profile":
                if row[key] != "":
                    social.update({'instagram': row[key]})
                    # social['instagram'] = row[key]
            
            if key == "Link to online ordering page":
                if row[key] != "":
                    links.update({'ordering': row[key]})
            if key == "Link to online reservation page":
                if row[key] != "":
                    links.update({'reservation': row[key]})
            if key == "Operating Hours":
                if row[key] != "":
                    """
                    tuesday Open: 17:00 Close: 21:00/, wednesday Open: 17:00 Close: 21:00/, thursday Open: 17:00 Close: 21:00/, friday, saturday, sunday Open: 17:00 Close: 21:00/
                    """
                    hours = []
                    operating_hours = str(row[key]).split("/")
                    for hour in operating_hours:
                        if hour != "":
                            """
                            tuesday Open: 17:00 Close: 21:00
                            , thursday Open: 17:00 Close: 21:00
                            """
                            if hour.startswith(", "):
                                hour = hour.replace(", ", "", 1)
                            temp = hour.split(', ')
                            days = temp[:-1]    # exclude last element
                            last_elm = temp[-1] # last element
                            last_elm = last_elm.replace("Open: ", "").replace("Close: ", "")
                            split_last_elm = last_elm.split(" ")
                            _day = " ".join(split_last_elm[:-2])
                            _open = split_last_elm[-2]
                            _close = split_last_elm[-1]
                            days.append(_day)
                            hours.append({
                                'days': days,
                                'open': _open,
                                'close': _close
                            })
                    obj.update({'hours': hours})
            if key == "Qualifying Type":
                if row[key] != "":
                    qualify_types = str(row[key]).split(', ')
                    for qt in qualify_types:
                        if qt == "Halal Certified":
                            verification.update({'hc': True})
                        elif qt == "Processing":
                            verification.update({'p': True})
                        elif qt == "Muslim Owner":
                            verification.update({'mo': True})
                        elif qt == "100% Muslim Owner":
                            verification.update({'fmo': True})
                        elif qt == "Halal Ingredients":
                            verification.update({'hi': True})
                        elif qt == "Halal Meat Only":
                            verification.update({'hmo': True})
                        elif qt == "User Generated":
                            verification.update({'ug': True})
                        elif qt == "Seafood Only":
                            verification.update({'so': True})
                        elif qt == "Vegetarian":
                            verification.update({'v': True})
                        elif qt == "Halal Meat Available":
                            verification.update({'hma': True})
                        elif qt == "Halal Ingredients Only":
                            verification.update({'hi': True})
                        elif qt == "No alcohol in meals":
                            verification.update({'na': True})
                        elif qt == "No Meat, No Alcohol":
                            verification.update({'nma': True})
                        elif qt == "Pork Free":
                            verification.update({'pf': True})
            if key == "Halal Certificate Expiry Date":
                if row[key] != "":
                    split_date = str(row[key]).split(" ")
                    day = split_date[0].replace("st", "").replace("nd", "").replace("rd", "").replace("th", "")
                    reformat_date = f"{day} {split_date[1]} {split_date[2]}"
                    try:
                        date = datetime.strptime(reformat_date, '%d %B %Y')
                        verification.update({"he": date.isoformat()})
                    except ValueError:
                        pass
            if key == "Halal Verification Expiry Date":
                if row[key] != "":
                    split_date = str(row[key]).split(" ")
                    day = split_date[0].replace("st", "").replace("nd", "").replace("rd", "").replace("th", "")
                    reformat_date = f"{day} {split_date[1]} {split_date[2]}"
                    try:
                        date = datetime.strptime(reformat_date, '%d %B %Y')
                        verification.update({"verificationExpiryDate": date.isoformat()})
                    except ValueError:
                        pass
            obj.update({'verification': verification})
            if key == "Discount offered":
                if row[key] != "":
                    reward.update({'discount': row[key]})
            if key == "Discount Code":
                if row[key] != "":
                    reward.update({'friendCode': row[key]})
            if key == "Terms":
                if row[key] != "":
                    reward.update({'terms': row[key]})
            obj.update({'reward': reward})
            if key == "Submitted By":
                if row[key] != "":
                    obj.update({'author': {
                        'createdBy': row[key]
                    }})
            if key == "Code":
                if row[key] != "":
                    obj.update({'code': row[key]})
            if key == "Options":
                if row[key] != "":
                    temp = str(row[key]).replace(', ', '|')  
                    sub_options = temp.split(',')     
                    for so in sub_options:
                        opts = so.split('|')
                        if opts[0] in diningTypes:
                            options.update({'diningTypes': opts})
                        elif opts[0] in placeTypes:
                            options.update({'placeTypes': opts})
                        elif opts[0] in features:
                            options.update({'features': opts})
                        elif opts[0] in catererServiceTypes:
                            options.update({'catererServiceTypes': opts})
                        elif opts[0] in catererType:
                            options.update({'catererType': opts})
                        elif opts[0] in eventTypes:
                            options.update({'eventTypes': opts})
                        elif opts[0] in orderingMethods:
                            options.update({'orderingMethods': opts})
                        elif opts[0] in onlineBasedMerchantTypes:
                            options.update({'onlineBasedMerchantTypes': opts})
                        elif opts[0] in onlineBasedServiceTypes:
                            options.update({'onlineBasedServiceTypes': opts})
                        elif opts[0] in pointOfSale:
                            options.update({'pointOfSale': opts})
                        elif opts[0] in productCategories:
                            options.update({'productCategories': opts})
                        elif opts[0] in suppliesServiceTypes:
                            options.update({'suppliesServiceTypes': opts})
                        elif opts[0] in deliveryLocations:
                            options.update({'deliveryLocations': opts})
                        elif opts[0] in bakeType:
                            options.update({'bakeType': opts})
                    obj.update({'options': options})
            if key == "Tags":
                if row[key] != "":
                    temp = str(row[key]).replace(', ', '|')  
                    sub_tags = temp.split(',')     
                    for st in sub_tags:
                        ts = st.split('|')
                        doc = tags_col.find_one({"values": {"$in": ts }})
                        if doc is not None:
                            tags.update({f"{doc.get('key')}": ts})
                    obj.update({'tags': tags})
            if key == "Halal Awards":
                if row[key] != "":
                    award_ids = []
                    awards = str(row[key]).split(', ')
                    for award in  awards:
                        doc = awards_col.find_one({"title": award})
                        if doc is not None:
                            # insert listings id to awards
                            # print(doc['title'])
                            award_ids.append(doc['_id'])
                    obj.update({'awards': award_ids})

        obj.update({'social': social})
        obj.update({'links': links})
        output_data_arr.append(obj)
    return output_data_arr


def process_users_export(db, input_data_arr) -> List[dict]:
    output_data_arr = []
    hashed_pasword = bcrypt.hashpw("12345678".encode(), bcrypt.gensalt(10))
    for row in input_data_arr:
        obj = {}
        profile = {}
        for key in row.keys():
            if key == "Email" and row[key] != "":
                obj.update({'emails': [
                    {
                        "address": row[key],
                        "verified": False
                    }
                ]})
            if key == "Name" and row[key] != "":
                profile.update({
                    'fullName': row[key]
                })
            if key == "Created At" and row[key] != "":
                split_date = str(row[key]).split(" ")
                day = split_date[0].replace("st", "").replace("nd", "").replace("rd", "").replace("th", "")
                reformat_date = f"{day} {split_date[1]} {split_date[2]}"
                reformat_time = f"{split_date[3]}:00"
                if split_date[4].upper() == "PM":
                    hour = int(reformat_time.split(':')[0])
                    hour += 12
                    if hour == 24:
                        hour = 0
                    reformat_time = f"{hour}:{reformat_time.split(':')[1]}:00"
                reformat_date = reformat_date + " " + reformat_time
                try:
                    _date = datetime.strptime(reformat_date, '%d %B %Y %H:%M:%S')
                    obj.update({"createdAt": _date.isoformat()})
                except ValueError as e:
                    print(str(e))
            if key == "Friend":
                pass
            if key == "Expiry Date" and row[key] not in ["", "null"]:
                split_date = str(row[key]).split(" ")
                day = split_date[0].replace("st", "").replace("nd", "").replace("rd", "").replace("th", "")
                reformat_date = f"{day} {split_date[1]} {split_date[2]}"
                try:
                    date = datetime.strptime(reformat_date, '%d %B %Y').timestamp()*1000
                    obj.update({"pass": {
                        'expiryDate': date
                    }})
                except ValueError:
                    pass
            if key == "IP Address" and row[key] != "":
                profile.update({"location": {
                    "ip": row[key]
                }})
        
        obj.update({'profile': profile})
        obj.update({'services': {
            'password': {
                'bcrypt': hashed_pasword.decode()
            }
        }})
        output_data_arr.append(obj)
        
    return output_data_arr


import typer

app = typer.Typer()

@app.command()
def index():
    print("Hello")

@app.command()
def listings(
    mongo_uri: str, 
    db_name: str,
    csv_path: str = "merchant-listings.csv", 
    json_path: str = "listings.json", 
    save: bool = False
):
    print(f"Connect {mongo_uri}")
    db = get_database(mongo_uri, db_name)
    print(f"Process {csv_path}")
    input = read_csv(csv_path)
    output = process_merchant_listings(db, input)
    if save:
        print(f"Save data to {json_path}.")
        save_json(json_path, output)
    else: 
        print(f"Check existence in database")
        listings_col = db['listings']
        awards_col = db['awards']
        exist_count = 0
        insert_count = 0
        
        for obj in output:
            query = {}
            if "code" in obj:
                query.update({'code': obj['code']})
            elif "slug" in obj:
                query.update({'slug': obj['slug']})
            else:
                query.update({'title': obj['title']})
        
            doc = listings_col.find_one(query, projection={ 'city': False })
            if doc is not None:
                exist_count += 1
                print(f"Document _id: {doc['_id']}, slug: {doc['slug']} already exist.")
            else:
                # insert...
                clone_obj = obj
                clone_obj.pop('awards', None)
                result = listings_col.insert_one(clone_obj)
                if "awards" in obj:
                    for award_id in obj['awards']:
                        awards_col.update_one({"_id": award_id}, {"$push": {"participants": result['_id']}})
                insert_count += 1
        print(f"Total {exist_count} document already exist.")
        print(f"Total {insert_count} new document insert to database.")

@app.command()
def users(
    mongo_uri: str, 
    db_name: str,
    csv_path: str = "users-export.csv", 
    json_path: str = "users.json", 
    save: bool = False
):
    print(f"Connect {mongo_uri}")
    db = get_database(mongo_uri, db_name)
    print(f"Process {csv_path}")
    input = read_csv(csv_path)
    output = process_users_export(db, input)
    if save:
        print(f"Save data to {json_path}.")
        save_json(json_path, output)
    else:
        print(f"Check existence in database")
        # 1,5 month ago
        time_ago = datetime.utcnow() - timedelta(days=47)
        users_col = db["users"]
        exist_count = 0
        insert_count = 0

        for obj in output:
            query = {}
            query.update({'emails.address': obj['emails'][0]['address']})

            # created = datetime.strptime(obj['createdAt'], "%Y-%m-%dT%H:%M:%S")
            # query.update({'createdAt': created.isoformat()})
            doc = users_col.find_one(query)
            if doc is not None:
                exist_count += 1
                print(f"Document _id: {doc['_id']}, email: {doc['emails'][0]['address']} already exist.")
            else:
                # print(obj['emails'][0]['address'])
                result = users_col.insert_one(obj)
                insert_count += 1
        print(f"Total {exist_count} document already exist.")
        print(f"Total {insert_count} new document insert to database.")
            


if __name__ == "__main__":
    app()