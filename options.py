features = [
    "Party Packs Available",
    "Carpark Nearby",
    "FRIENDS Rewards",
    "Delivery Available",
    "Musollah Available nearby",
    "Breastfeeding Room",
    "Kid's Menu Available",
    "Healthy Options Available",
    "Wi-fi Available",
    "Wedding Bookings",
    "Reservations Available",
    "Private Functions Booking",
    "MRT within walking distance",
    "For Business Meetings",
    "For Birthday Parties",
    "For Big Groups",
    "Bus Stop within walking distance",
    "Air-Conditioning",
    "24 Hours",
]

placeTypes = [
    "Take-Out",
    "Dine-in",
]

diningTypes = [
    "Food Court",
    "Hawker Stall",
    "Others",
    "Grab n Go",
    "Alfresco",
    "Bakery",
    "Restaurant",
    "Cafeteria",
    "Cafe-Bistro",
    "Buffet",
    "Container Park"
    "Event Venue"
    "Café"
    "Retail Store"
]

bakeType = [
    'Bakery',
    'Confectionery',
    'SFA Licensed Kitchen',
    'Home-based',
    'Baking Classes',
]

deliveryLocations = [
    
        'Central',
    
    
        'East',
    
    
        'North',
    
    
        'North East',
    
    
        'West',
    
    
        'Island-wide',
    
    
        'None',
    
]

productCategories = [
    
        'Cakes',
    
    
        'Cookies',
    
    
        'Chocolates',
    
    
        'Tarts',
    
    
        'Traditional Kueh',
    
    
        'Puffs',
    
    
        'Wedding',
    
    
        'Customised Cakes',
    
    
        'Baking Classes',
    
]

catererType = [
  
    'Live Station',
  
  
    'Events',
  
  
    'Corporate',
  
  
    'Wedding',
  
]

catererServiceTypes = [
    
        'Party Packs',
    
    
        'Venue Decor Service Available',
    
    
        'Event Venue Available',
    
    
        'Live Stations',
    
    
        'Mini Catering (5 to 10 pax)',
    
    
        'Mini Buffet Set Up',
    
    
        'Full Buffet Set Up',
    
    
        'Tingkat Meals',
    
    
        'Bento Sets',
    
]

eventTypes = [
    
        'Corporate Events',
    
    
        'Seminar',
    
    
        'Wedding',
    
    
        'House Warming',
    
    
        'Baby Shower',
    
    
        'Birthday Parties',
    
    
        'Cocktail & Tea Reception',
    
]

onlineBasedMerchantTypes = [
    
        'Pop-Up Booth',
    
    
        'Home Based Baker',
    
    
        'Home Based Cook',
    
    
        'Registered Kitchen',
    
    
        'Caterer',
    
    
        'Food Delivery',
    
    
        'Events Live Stations',
    
    
        'Product Retail',
    
]

onlineBasedServiceTypes = [
    
        'Delivery',
    
    
        'Self Pick Up',
    
    
        'Pop Up Booth',
    
    
        'On-site Set-Up',
    
    
        'Wedding Bookings',
    
]

orderingMethods = [
    
        'Visit our Pop Up Booth',
    
    
        'Order via Facebook',
    
    
        'Order via Instagram',
    
    
        'Order via App',
    
    
        'Order via Email',
    
    
        'Sms or Whatsapp to order',
    
    
        'Website',
    
    
        'Call to Order',
    
]

suppliesServiceTypes = [
    
        'Wet Market',
    
    
        'Supermarket',
    
    
        'Hawker',
    
]

pointOfSale = [
    
        'Physical Outlet',
    
    
        'Online-based',
    
]