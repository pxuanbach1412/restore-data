## Required

- Python >= 3.7

## Create virtual environment

```bash
python -m venv venv
# or
py -m venv venv
```

## Activate virtual environment

Windows:
```bash
venv\Scripts\activate
```

Linux/Mac:
```bash
source venv/bin/activate
```

## Install package

```bash
pip install -r requirements.txt
```

## Use

```bash
python main.py --help

# common
python main.py listings "mongo://..." "db_name" --csv-path "/root/..."
python main.py users "mongo://..." "db_name" --csv-path "/root/..."
```